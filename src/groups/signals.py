from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

from groups.models import Group
from lessons.models import Lesson


# @receiver(post_save, sender=Group.subjects.through)
# def save_group(sender, instance, action, **kwargs):
#     if action == "post_add":
#         subjects_after_changes = instance.subjects.filter()
#         lessons = Lesson.objects.filter(group=instance)
#         lessons.exclude(subject__in=subjects_after_changes).delete()
#         for subject in instance.subjects.exclude():
#             for _ in range(instance.semester_weeks):
#                 obj = Lesson(
#                     subject=subject,
#                     group=instance,
#                 )
#                 obj.save()
