from django.urls import path, include

from groups.views import StudentJournalView, SubjectStudentJournalView, StudentGroupView, StudentListView, \
    AdminModerateGroup, JournalSelectView, TeacherJournalUpdateView, AdminModerateStudentsGroup

app_name = 'groups'

urlpatterns = [
    path('<uuid:group_uuid>/', StudentGroupView.as_view(), name='group'),
    path('moderate/', StudentListView.as_view(), name='moderate'),
    path('moderate/<uuid:group_uuid>/schedule/', AdminModerateGroup.as_view(), name='moderate_group'),
    path('moderate/<uuid:group_uuid>/students/', AdminModerateStudentsGroup.as_view(), name='moderate_group_students'),
    path('<uuid:group_uuid>/schedule/', StudentJournalView.as_view(), name='group_schedule_for_student'),
    path('<uuid:group_uuid>/journal/', StudentJournalView.as_view(), name='group_schedule_for_student'),
    path('journal/', JournalSelectView.as_view(), name='select_journal'),
    path('journal/<uuid:group_uuid>/teacher/subject/<uuid:subject_uuid>/', TeacherJournalUpdateView.as_view(),
         name='group_journal_for_teacher'),
    path('journal/<uuid:group_uuid>/student/subject/<uuid:subject_uuid>/', SubjectStudentJournalView.as_view(),
         name='subject_journal_for_student'),

]
