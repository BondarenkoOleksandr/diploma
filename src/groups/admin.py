from django.contrib import admin

from groups.models import Group


class GroupAdmin(admin.ModelAdmin):
    readonly_fields = ['uuid']
    exclude = ['headman']


admin.site.register(Group, GroupAdmin)
