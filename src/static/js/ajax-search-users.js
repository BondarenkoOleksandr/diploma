function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


//function sendJournalMarks() {
//    document.querySelectorAll('.shedule__modal--item input').forEach(item => {
//        item.addEventListener('change', (e) => {
//            const request = new XMLHttpRequest();
//            const url = e.target.dataset.action;
//            const csrftoken = getCookie('csrftoken');
//            const params = "csrfmiddlewaretoken=" + csrftoken+'&term='+e.target.value;
//            console.log(params)
//
//            //	Здесь нужно указать в каком формате мы будем принимать данные вот и все	отличие
//            request.responseType =	"json";
//            request.open("GET", url, true);
//            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//
//            request.addEventListener("readystatechange", () => {
//
//                if (request.readyState === 4 && request.status === 200) {
//                    let obj = request.response;
//                    console.log(obj)
//                }
//            });
//
//            request.send(params);
//        })
//    })
//}
//sendJournalMarks();
$(document).ready(function () {
    var csrftoken = getCookie('csrftoken');
    $(".userSearch").on('input', function () {
        $.ajax({
            type: 'GET',
            async: true,
            url: $('.userSearch').data('action'),
            data: "csrftoken="+csrftoken+"&term="+$('.userSearch').val(),
            success: function(data) {
            const isEmpty = x => !Object.keys(x).length;
            var len = Object.keys(data).length
            if( !isEmpty(data) ){
                var k=''
                for ( var i = 0; i < len; i++){
                    $(data[i]).each(function(i, data) {
                         k += `
                                <div class="search__item">
                                    <div class="search__result--name">
                                        ${data.full_name}
                                    </div>
                                        <div class="search__result--btn" data-action="/accounts/update/${data.id}"><img class="img-svg" src="${plus_src}" alt=""></div>
                                </div>`;
                    })
                    }
                $('.search__block').html(k);
            }
            },
            dataType: 'json',
        });
    });
});