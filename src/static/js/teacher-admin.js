function addNewTeacher() {
    let btn = document.querySelector('.group__buttons--add'),
        modal = document.querySelector('.shedule__teacher'),
        close = document.querySelector('.shedule__modal--close');
    if (btn) {
        btn.addEventListener('click', () => {
            document.querySelector('.shedule__modal--cover').classList.remove('subjectModalOpen');
            modal.classList.add("subjectModalOpen");
        });
        close.addEventListener('click', (e) => {
            modal.classList.remove("subjectModalOpen");
        });
    }
}
addNewTeacher();
