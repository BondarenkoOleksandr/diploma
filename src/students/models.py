from django.db import models

# Create your models here.
from core.mixins import FieldPermissionModelMixinV3
from lessons.utils import create_marks


class Student(FieldPermissionModelMixinV3, models.Model):

    __original_group = None

    user = models.OneToOneField('accounts.User', on_delete=models.CASCADE, primary_key=True)

    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.SET_NULL,
        related_name='students',
    )

    class Meta:
        permissions = (
            ('can_change_student_group', "Can change group"),
        )

    def full_name(self):
        return f'{self.user.last_name} {self.user.first_name}'

    def __init__(self, *args, **kwargs):
        super(Student, self).__init__(*args, **kwargs)
        self.__original_name = self.group

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.group != self.__original_name:
            create_marks(self)

        super(Student, self).save(force_insert, force_update, *args, **kwargs)
        # self.__original_name = self.name

    def __str__(self):
        return f'{self.user.surname} {self.user.first_name}'
