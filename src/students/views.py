from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from students.forms import StudentCreateForm, StudentUpdateForm
from students.models import Student


class StudentsListView(PermissionRequiredMixin, LoginRequiredMixin, ListView):
    permission_required = 'students.view_student'
    model = Student
    template_name = 'students/students-list.html'
    context_object_name = 'students'

    def get_object(self):
        students = Student.objects.all().values('user__id','user__first_name', 'user__last_name', 'user__surname', 'user__email',
                                                'user__phone_number').order_by('user__last_name', 'user__first_name')
        return students

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object()

        return context


class StudentCreateView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    permission_required = 'students.create_student'
    model = Student
    form_class = StudentCreateForm
    template_name = 'students/student-create.html'
    success_url = reverse_lazy('index')


class StudentUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'students.change_student'
    model = Student
    form_class = StudentUpdateForm
    template_name = 'students/student-update.html'
    success_url = reverse_lazy('index')


class StudentDeleteView(PermissionRequiredMixin, LoginRequiredMixin, DeleteView):
    permission_required = 'students.delete_student'
    model = Student
    template_name = 'students/student-delete.html'
    success_url = reverse_lazy('index')
    pk_url_kwarg = 'student_id'
