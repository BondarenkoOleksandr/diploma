let modal = document.querySelector('.shedule__modal'),
    btn = document.querySelectorAll('.shedule__subject--mini'),
    close = document.querySelector('.shedule__modal--close'),
    edit = document.querySelectorAll('.shedule__subject--edit'),
    title = document.querySelector('.shedule__modal--title'),
    modalCover = document.querySelector('.shedule__modal--cover'),
    loadingImage = document.querySelector('.cap__image--block'),
    successImage = document.querySelector('.shedule__modal--check'),
    modalError = document.querySelector('.shedule__modal--error'),
    inputRoom = document.getElementsByName('room')[0],
    formMain = document.querySelector('.shedule__modal--main form'),
    checkmark = document.querySelector('.shedule__modal--check svg'),
    changedBlock,
    form;

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function addSubject() {
    if (btn) {
        btn.forEach(function(item) {
            item.addEventListener('click', function() {
                openModal('update-lesson', 'create-lesson', CreateLesson, item, "Додати новий предмет");
                clearData();
            });
        });
        closeModal();        

        function CreateLesson(evt){
            evt.preventDefault();
            modalCover.classList.add('subjectModalOpen');
            loadingImage.style.cssText = "display: block; z-index: 10000";
            console.log(evt);
            const request = new XMLHttpRequest();
            const url = evt.target.dataset.action;
            console.log(url);
            const csrftoken = getCookie('csrftoken');
            var params = "csrfmiddlewaretoken=" + csrftoken;
            inputs = form.elements;
            for (i = 0; i < inputs.length; i++) {
                params+='&' +inputs[i].name +'='+inputs[i].value;
            }
            request.responseType =	"json";
            request.open("POST", url, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
            request.addEventListener("readystatechange", () => {
                let modalError = document.querySelector('.shedule__modal--error');
                if (request.readyState === 4 && request.status === 200) {
                    let obj = request.response;
                    console.log(obj);
                    // Проверка на наличие четной/нечетной недели
                    if (obj.mode == 1 || obj.mode == 2) {
                        let typeBlock = changedBlock.parentElement.parentElement,
                            secondSubject = changedBlock.parentElement.parentElement.nextElementSibling;
                        typeBlock.classList.add('shedule__mini');
                        if (typeBlock.nextElementSibling) {
                            typeBlock.nextElementSibling.classList.remove('shedule__hide');
                        } else {
                            typeBlock.previousElementSibling.classList.remove('shedule__hide')
                        }
                        if (secondSubject) {
                            console.log(secondSubject)
                            if (!secondSubject.classList.contains('shedule__mini')) {
                                secondSubject.classList.remove('shedule__hide');
                                secondSubject.classList.add('shedule__mini');
                                changedBlock.parentElement.parentElement.nextElementSibling.childNodes[1].style.cssText = "visibility: visible; opacity: 1; z-index: 10";
                                changedBlock.parentElement.parentElement.nextElementSibling.childNodes[1].nextElementSibling.style.cssText = "visibility: hidden; opacity: 0; z-index: -10";
                            }
                            else {
                                if (changedBlock.parentElement.parentElement.childNodes[2].nextElementSibling) {
                                changedBlock.parentElement.parentElement.childNodes[2].nextElementSibling.style.cssText = "visibility: visible; opacity: 1; z-index: 10";
                                }
                            }
                        }
                    } else if (obj.mode == 0) {
                        let typeBlock = changedBlock.parentElement.parentElement;
                        console.log(typeBlock)
                        typeBlock.classList.remove('shedule__mini')
                        if (typeBlock.nextElementSibling) {
                            typeBlock.nextElementSibling.classList.add('shedule__hide')
                        } else {
                            typeBlock.previousElementSibling.classList.add('shedule__hide')
                        }
                    }
                    closeModalAndSetData('.shedule__subject--plus');
                    let edit = changedBlock.parentElement.nextElementSibling.nextElementSibling.childNodes[1].childNodes[1],
                        del = changedBlock.parentElement.nextElementSibling.nextElementSibling.childNodes[1].childNodes[3];
                    
                    edit.dataset.action = '/lessons/update/' + request.response.lesson_uuid+'/';
                    edit.dataset.group = request.response.group;
                    edit.dataset.day = request.response.days;
                    edit.dataset.time = request.response.time;
                    del.dataset.action = '/lessons/delete/' + request.response.lesson_uuid+'/';
                } else if (request.status=500){
                    console.log(request.response.__all__[0].message);
                    modalCover.classList.remove('subjectModalOpen');
                    loadingImage.style.cssText = "display: none; z-index: -10";
                    modalError.innerHTML = request.response.__all__[0].message;
                    modalError.style.display = 'block';
                }
            });
            request.send(params);
        }
    }
}
addSubject();

function editSubject() {
    if (edit) {
        edit.forEach(function(item) {
            item.addEventListener('click', function() {
                openModal('create-lesson', 'update-lesson', UpdateLesson, item, "Редагувати предмет");
                setData(item);
            })
        })
        closeModal();
        
        function UpdateLesson(evt){
            evt.preventDefault();
            modalCover.classList.add('subjectModalOpen');
            loadingImage.style.cssText = "display: block; z-index: 10000";
            const request = new XMLHttpRequest();
            const url = evt.target.dataset.action;
            console.log(url);
            const csrftoken = getCookie('csrftoken');
            var params = "csrfmiddlewaretoken=" + csrftoken;
            inputs = form.elements;
            for (i = 0; i < inputs.length; i++) {
                params+='&' +inputs[i].name +'='+inputs[i].value;
            }
        
            request.responseType =	"json";
            request.open("POST", url, true);
            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        
            request.addEventListener("readystatechange", () => {
                let modalError = document.querySelector('.shedule__modal--error');
                if (request.readyState === 4 && request.status === 200) {
                    let obj = request.response;
                    // Проверка на наличие четной/нечетной недели
                    if (obj.mode == 1 || obj.mode == 2) {
                        let typeBlock = changedBlock.parentElement.parentElement,
                            secondSubject = changedBlock.parentElement.parentElement.nextElementSibling;
                        typeBlock.classList.add('shedule__mini');
                        if (secondSubject) {
                            console.log(secondSubject)
                            if (!secondSubject.classList.contains('shedule__mini')) {
                                secondSubject.classList.remove('shedule__hide');
                                secondSubject.classList.add('shedule__mini');
                                changedBlock.parentElement.parentElement.nextElementSibling.childNodes[1].style.cssText = "visibility: visible; opacity: 1; z-index: 10";
                                changedBlock.parentElement.parentElement.nextElementSibling.childNodes[1].nextElementSibling.style.cssText = "visibility: hidden; opacity: 0; z-index: -10";
                            }
                            else {
                                if (changedBlock.parentElement.parentElement.childNodes[2].nextElementSibling) {
                                changedBlock.parentElement.parentElement.childNodes[2].nextElementSibling.style.cssText = "visibility: visible; opacity: 1; z-index: 10";
                                }
                            }
                        }
                    }
                    closeModalAndSetData('.shedule__subject--change');
                    let edit = changedBlock.parentElement.closest('.shedule__subject--change').childNodes[1].childNodes[1],
                    del = changedBlock.parentElement.closest('.shedule__subject--change').childNodes[1].childNodes[3];

                    edit.dataset.action = '/lessons/update/' + request.response.lesson_uuid+'/';
                    edit.dataset.group = request.response.group;
                    edit.dataset.day = request.response.days;
                    edit.dataset.time = request.response.time;
                    del.dataset.action = '/lessons/delete/' + request.response.lesson_uuid+'/';
                } else if (request.status=500) {
                    console.log(request.response.__all__[0].message);
                    modalCover.classList.remove('subjectModalOpen');
                    loadingImage.style.cssText = "display: none; z-index: -10";
                    modalError.innerHTML = request.response.__all__[0].message;
                    modalError.style.display = 'block';
                }
            });
            request.send(params);
        }
    }
}
editSubject();

function openModal(id, newId, submitFunction, item, title) {
    title.textContent = title;
    if(document.getElementById(id)){
        document.getElementById(id).id = newId;
    }
    form = document.getElementById(newId);
    if (form) {
        form.addEventListener('submit', submitFunction);
    } 
    changedBlock=item;
    modalCover.classList.remove('subjectModalOpen')
    document.querySelector("input[name='days']").value = item.dataset.day;
    document.querySelector("input[name='time']").value = item.dataset.time;
    document.querySelector("input[name='group']").value = item.dataset.group;
    document.querySelector(`#${newId}`).dataset.action = item.dataset.action;
    modal.classList.add("subjectModalOpen");
}

function setData(item) {
    infoBlock = item.parentElement.closest('.shedule__subject--change').previousElementSibling;
    let subjectName = infoBlock.childNodes[3].textContent.trim(),
        subjectTeacher = infoBlock.childNodes[5].textContent.trim(),
        subjectRoom = infoBlock.childNodes[1].childNodes[1].textContent.trim(),
        subjectType = infoBlock.childNodes[1].childNodes[3].textContent.trim(),
        infoRooms = document.getElementsByName('room')[0];
    infoRooms.value = subjectRoom;
    $("select option:selected").removeAttr("selected")    
    $("select[name='type']").find(`option:contains(${subjectType})`).attr("selected", "selected");
    $("select[name='subject']").find(`option:contains(${subjectName})`).attr("selected", "selected");
    $("select[name='teacher_field']").find(`option:contains(${subjectTeacher})`).attr("selected", "selected");
}

function clearData() {
    document.getElementsByName('room')[0].value = '';
    document.querySelector('.shedule__modal--error').style.display = 'none';

    $('select').prop('selectedIndex',0);
}

function closeModalAndSetData(siblingBlock) {
    let infoBlock;
    if (changedBlock.parentElement.closest(`${siblingBlock}`).previousElementSibling != undefined) {
        infoBlock = changedBlock.parentElement.closest(`${siblingBlock}`).previousElementSibling;
    } else if (changedBlock.parentElement.closest(`${siblingBlock}`).previousElementSibling == undefined) {
        infoBlock = changedBlock.parentElement.closest(`${siblingBlock}`).nextElementSibling;
    }
    let subjectName = infoBlock.childNodes[3],
        subjectTeacher = infoBlock.childNodes[5],
        subjectRoom = infoBlock.childNodes[1].childNodes[1],
        subjectType = infoBlock.childNodes[1].childNodes[3];
    subjectRoom.textContent = inputRoom.value;
    subjectType.textContent = document.getElementsByName('type')[0].options[document.getElementsByName('type')[0].selectedIndex].text;
    subjectTeacher.textContent = document.getElementsByName('teacher_field')[0].options[document.getElementsByName('teacher_field')[0].selectedIndex].text;;
    subjectName.textContent = document.getElementsByName('subject')[0].options[document.getElementsByName('subject')[0].selectedIndex].text;;
    
    // Скрытие плюса при добавлении элемента
    if (changedBlock.parentElement.closest('.shedule__subject--plus')) {
        changedBlock.parentElement.closest('.shedule__subject--plus').style.cssText = "opacity: 0; visibility: hidden; z-index: -4";
    }
    // Добавление классов реагирующих на наведения и показ блока редактирования
    infoBlock.classList.add('current');
    changedBlock.parentElement.parentElement.classList.add('hoverChange')
    // Запуск анимации
    let anim = "animate";
        successImage.style.zIndex = "20";
    if (!checkmark.classList.contains(anim)) {
        loadingImage.style.cssText = "display: none; z-index: -10";
        checkmark.classList.add(anim);
        infoBlock.style.cssText = "opacity: 1; visibility: visible; z-index: 4";
        setTimeout(function(){
            checkmark.classList.remove(anim);
            successImage.style.zIndex = "-10";
            modal.classList.remove("subjectModalOpen");
            var new_element = form.cloneNode(true);
            form.parentNode.replaceChild(new_element, form)
        }, 1700);
    }
}

function closeModal() {
    close.addEventListener('click', (e) => {
        var new_element = formMain.cloneNode(true);
        formMain.parentNode.replaceChild(new_element, formMain)
        modal.classList.remove("subjectModalOpen");
        $("select option:selected").prop('selectedIndex', 2)
    })
}

function deleteSubject() {
    let modal = document.querySelector('.shedule__delete'),
        btn = document.querySelectorAll('.shedule__subject--delete'),
        yesBtn = document.querySelector('.shedule__delete--yes'),
        noBtn = document.querySelector('.shedule__delete--no'),
        plus = document.querySelector('.shedule__subject--plus'),
        deletedBlock;
        if (btn) {
            btn.forEach(function(item) {
                item.addEventListener('click', function() {
                    deletedBlock=item;
                    modalCover.classList.remove('subjectModalOpen');
                    modal.classList.add("subjectModalOpen");
                })
            })
        }
        if (noBtn) {
            noBtn.addEventListener('click', function() {
                modalCover.classList.add('subjectModalOpen');
                modal.classList.remove("subjectModalOpen");
            })
        }
        if (yesBtn) {

            yesBtn.addEventListener('click', function() {
                console.log(deletedBlock)
                console.log()
                let elem = deletedBlock.parentElement.closest('.shedule__type')
                queryDeleteSubject(deletedBlock.dataset.action)
                plus.style.display = "opacity: 1; z-index: 10; visibility: visible";
                modalCover.classList.add('subjectModalOpen');
                modal.classList.remove("subjectModalOpen");
                let plusBlock = deletedBlock.parentElement.closest('.shedule__subject--change').previousElementSibling.previousElementSibling;
                plusBlock.style.cssText = "opacity: 1; z-index: 10; visibility: visible";
                let infoBlock = deletedBlock.parentElement.closest('.shedule__subject--change').previousElementSibling;
                infoBlock.style.cssText = "visibility: hidden; opacity: 0; z-index: -10";
//                if () {
//
//                }
            })
        }
}
deleteSubject();

function queryDeleteSubject(action_link){
    const request = new XMLHttpRequest();
    const url = action_link;
    console.log(url);
    const csrftoken = getCookie('csrftoken');
    var params = "csrfmiddlewaretoken=" + csrftoken;

    request.responseType =	"json";
    request.open("POST", url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", () => {

        if (request.readyState === 4 && request.status === 200) {
            let obj = request.response;
            closeModalAndSetData('.shedule__subject--change');
        }
    });

    request.send(params);
}