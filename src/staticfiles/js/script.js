window.btoa(
    window.addEventListener('DOMContentLoaded', () => {
        $preloader = $('.preloader'),
        $loader = $preloader.find('.preloadImg');
        $loader.delay(3550).fadeOut('slow');
        $preloader.delay(2550).fadeOut('slow');

        function setTableHeight() {
            if (document.querySelector('.journal__table')) {
                document.querySelector('.journal__table').style.maxHeight = document.body.scrollHeight - document.querySelector('.journal__table').offsetTop - 220 + 'px';
            }
            if (document.querySelector('.shedule__table')) {
                $('.shedule__table').css('maxHeight', $(window).height() - $('.shedule__table').offset().top - 40);
            }
        }

        function hideTabMenu() {
            let btn = document.querySelector('.header__tabHide'),
                menu = document.querySelector('.menu'),
                content = document.querySelector('.content'),
                menuItem = document.querySelectorAll('.menu__item'),
                menuList = document.querySelectorAll('.menu__list a'),
                menuTitle = document.querySelector('.menu__title'),
                menuLogo = document.querySelector('.menu__logo');
            btn.addEventListener('click', () => {
                console.log('click')
                if (!btn.classList.contains('hideTabMenuActive')) {
                    console.log('нет')
                    btn.classList.add('hideTabMenuActive');
                    menu.style.width = '80px';
                    content.style.marginLeft = '80px';
                    menuList.forEach(item => {
                        console.log(item.childNodes[2].nextSibling)
                        item.childNodes[2].nextSibling.style.display = 'none';
                    });
                    menuTitle.style.display = 'none';
                    menuLogo.style.display = 'block';
                    menuItem.forEach(item => {
                        item.classList.add('smallMenuItem');
                    })
                } else {
                    console.log('есть')
                    btn.classList.remove('hideTabMenuActive');
                    menu.style.width = '330px';
                    content.style.marginLeft = '330px';
                    menuList.forEach(item => {
                        console.log(item.childNodes[2].nextSibling)
                        item.childNodes[2].nextSibling.style.display = 'inline-block';
                    });
                    menuTitle.style.display = 'none';;
                    menuLogo.style.display = 'block';
                    menuItem.forEach(item => {
                        item.classList.remove('smallMenuItem');
                    })
                }
            })
        }
        hideTabMenu();

        onload = function() {
            var lnk = document.querySelectorAll('.menu__item');
            let currentLink = window.location.pathname;
            currentLink = currentLink.split('/')[1];
            for (var j = 0; j < lnk.length; j++) {
                if (lnk[j].href.includes(currentLink) || lnk[j].href.includes(window.location.pathname)) {
                    lnk[j].classList.add('menu__item--active');
                }
            }
        }
        function avatarChange() {
            let changeItem = document.querySelector('.change__edit');
            if (changeItem) {
                changeItem.onchange = function (evt) {
                    var tgt = evt.target || window.event.srcElement,
                        files = tgt.files;
    
                    // FileReader support
                    if (FileReader && files && files.length) {
                        var fr = new FileReader();
                        fr.onload = function () {
                            document.querySelector('.change__photo').src = fr.result;
                            document.querySelector('.header__icon img').src = fr.result;
                        }
                        fr.readAsDataURL(files[0]);
                    }
                }
            }
        }
        avatarChange();
        // $('select').each(function(){
        //     var $this = $(this), numberOfOptions = $(this).children('option').length;
          
        //     $this.addClass('select-hidden'); 
        //     $this.wrap('<div class="select"></div>');
        //     $this.after('<div class="select-styled"></div>');
        
        //     var $styledSelect = $this.next('div.select-styled');
        //     $styledSelect.text($this.children('option').eq(0).text());
          
        //     var $list = $('<ul />', {
        //         'class': 'select-options'
        //     }).insertAfter($styledSelect);
          
        //     for (var i = 0; i < numberOfOptions; i++) {
        //         $('<li />', {
        //             text: $this.children('option').eq(i).text(),
        //             rel: $this.children('option').eq(i).val()
        //         }).appendTo($list);
        //     }
          
        //     var $listItems = $list.children('li');
          
        //     $styledSelect.click(function(e) {
        //         e.stopPropagation();
        //         $('div.select-styled.active').not(this).each(function(){
        //             $(this).removeClass('active').next('ul.select-options').hide();
        //         });
        //         $(this).toggleClass('active').next('ul.select-options').toggle();
        //     });
          
        //     $listItems.click(function(e) {
        //         e.stopPropagation();
        //         $styledSelect.text($(this).text()).removeClass('active');
        //         $this.val($(this).attr('rel'));
        //         $list.hide();
        //         //console.log($this.val());
        //     });
          
        //     $(document).click(function() {
        //         $styledSelect.removeClass('active');
        //         $list.hide();
        //     });
        
        // });
        
        // $('select').selectize({
        //     create: true,
        //     sortField: 'text'
        // });
    function menu() {
        if (document.querySelector(".header__menu")) {
            document.querySelector(".header__menu").addEventListener('click', () => {
                document.querySelector(".menu").style.right = "0";
            });
            document.querySelector(".menu__close").addEventListener('click', () => {
                document.querySelector(".menu").style.right = "-100%";
            });
        }
    }
    menu();

    jQuery(document).ready(function() {
        jQuery(".journal-table").clone(true).appendTo('#journal-scroll').addClass('clone');   
        jQuery(".shedule-table").clone(true).appendTo('#shedule-scroll').addClass('clone');   
    });

    (function ($) {
        "use strict";
    
    
        /*==================================================================
        [ Focus Contact2 ]*/
        $('.register__input').each(function(){
            $(this).on('blur', function(){
                if($(this).val().trim() != "") {
                    $(this).addClass('has-val');
                }
                else {
                    $(this).removeClass('has-val');
                }
            })    
        })
      
      
        /*==================================================================
        [ Validate ]*/
        var input = $('.validate-input .input100');
    
        $('.validate-form').on('submit',function(){
            var check = true;
    
            for(var i=0; i<input.length; i++) {
                if(validate(input[i]) == false){
                    showValidate(input[i]);
                    check=false;
                }
            }
    
            return check;
        });
    
    
        $('.validate-form .input100').each(function(){
            $(this).focus(function(){
               hideValidate(this);
            });
        });
    
        function validate (input) {
            if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
                if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                    return false;
                }
            }
            else {
                if($(input).val().trim() == ''){
                    return false;
                }
            }
        }
    
        function showValidate(input) {
            var thisAlert = $(input).parent();
    
            $(thisAlert).addClass('alert-validate');
        }
    
        function hideValidate(input) {
            var thisAlert = $(input).parent();
    
            $(thisAlert).removeClass('alert-validate');
        }
    
    function navOptShow() {
        let image = document.querySelector('.header__icon img'),
            navBlock = document.querySelector('.header__option'),
            navBlockItem = document.querySelector('.header__option--item');
        if (image) {
            image.addEventListener('click', () => {
                if (navBlock.classList.contains('navAdd')) {
                    navBlock.style.cssText = "z-index: -2; opacity: 0; visibility: hidden";
                    navBlock.classList.remove('navAdd');
                } else {
                    navBlock.style.cssText = "z-index: 10; opacity: 1; visibility: visible";
                    navBlock.classList.add('navAdd');
                }
            })
            document.body.addEventListener('click', (e) => {
                if (e.target !=image && e.target !=navBlock && e.target !=navBlockItem) {
                    navBlock.style.cssText = "z-index: -2; opacity: 0; visibility: hidden";
                    navBlock.classList.remove('navAdd');
                }
            });
        }
        
    }
    navOptShow();

    function addNewSubject() {
        let modal = document.querySelector('.shedule__modal'),
            btn = document.querySelectorAll('.shedule__subject--mini'),
            close = document.querySelector('.shedule__modal--close')
        if (document.querySelectorAll('.shedule__subject--mini').length > 1) {
            btn.forEach((item) => {
                item.addEventListener('click', () => {
                    document.querySelector("input[name='days']").value = item.dataset.day;
                    document.querySelector("input[name='time']").value = item.dataset.time;
                    modal.classList.add("subjectModalOpen")
                })
            })
            close.addEventListener('click', (e) => {
                modal.classList.remove("subjectModalOpen")
            })
        }
        
    }
    addNewSubject();
        
    })(jQuery);
    
        jQuery(function($){
            $("#tel").mask("+7(999) 999-9999");
        });
    
        $('img.img-svg').each(function(){
            var $img = $(this);
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');
            $.get(imgURL, function(data) {
                var $svg = $(data).find('svg');
                if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
                }
                $svg = $svg.removeAttr('xmlns:a');
                if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                }
                $img.replaceWith($svg);
            }, 'xml');
        });
    
        
        
    })
)
