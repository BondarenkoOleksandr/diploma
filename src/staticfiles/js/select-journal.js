function openTeacherMarkBook() {
        let btn = document.querySelector('.shedule__item--openMarkBook'),
            discipline = document.querySelector('.shedule__item--discipline select');
        if (btn) {
            btn.addEventListener('click', (e) => {
                discipline = discipline.options[discipline.selectedIndex].value;
                if (document.querySelector('.shedule__item--group select')){
                    group = document.querySelector('.shedule__item--group select');
                    group = group.options[group.selectedIndex].value + '/teacher';
                }else{
                    group = document.querySelector('.shedule__item--current-group')
                    group = group.value + '/student';
                }
                e.preventDefault()
                window.location.href='/groups/journal/'+group+'/subject/'+discipline+'/'
            })
        }
    }
    openTeacherMarkBook();