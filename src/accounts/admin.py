from django.contrib import admin

# Register your models here.
from accounts.forms import UserChangeAdminForm
from accounts.models import User


class UserAdmin(admin.ModelAdmin):
    model = User
    readonly_fields = ('is_student', 'is_teacher')
    form = UserChangeAdminForm


admin.site.register(User, UserAdmin)
