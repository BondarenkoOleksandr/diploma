from accounts import roles
from groups.models import Group

from students.models import Student
from teachers.models import Teacher


def change_auth_group(model, groups, user_group_uuid):
    if groups.first() == roles.TEACHERS:
        model.groups.clear()
        model.groups.add(roles.TEACHERS)
        model.is_student = False
        model.is_teacher = True
        model.save()

        if not Teacher.objects.filter(user=model):
            Student.objects.filter(user=model).delete()
            Teacher.objects.create(user=model)

    elif groups.first() == roles.STUDENTS:
        model.groups.clear()
        model.groups.add(roles.STUDENTS)
        model.is_student = True
        model.is_teacher = False
        model.save()
        if not Student.objects.filter(user=model):
            Teacher.objects.filter(user=model).delete()
            student_group = Group.objects.get(uuid=user_group_uuid)
            Student.objects.create(user=model,group=student_group)

        else:
            student = Student.objects.get(user=model)
            student.group = Group.objects.get(uuid=user_group_uuid)
            student.save()

    return model
