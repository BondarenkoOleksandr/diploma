from PIL import Image
from django.db import models
from django.contrib.auth.models import AbstractUser

from accounts.validators import validate_phone_number


class User(AbstractUser):
    birth_date = models.DateField(null=True, blank=True)
    surname = models.CharField(max_length=210, blank=True)
    phone_number = models.CharField(null=True,
                                    max_length=20,
                                    validators=[validate_phone_number],
                                    default="+38(098)000-0000")
    image = models.ImageField(default='default.png', upload_to='accounts')
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        image = Image.open(self.image)
        image.thumbnail((300, 300), Image.ANTIALIAS)
        image.save(self.image.path)

    def get_group_uuid(self):
        return self.student.group.uuid

    def in_group(self):
        return bool(self.student.group)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
