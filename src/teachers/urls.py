from django.urls import path

from teachers.views import TeacherDeleteView, TeacherCreateView, TeacherUpdateView, TeachersListView

app_name = 'teachers'

urlpatterns = [
    path('create/', TeacherCreateView.as_view(), name='create'),
    path('update/', TeacherUpdateView.as_view(), name='update'),
    path('delete/<int:teacher_id>', TeacherDeleteView.as_view(), name='delete'),
    path('list/', TeachersListView.as_view(), name='list'),
]
