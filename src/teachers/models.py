from django.db import models

# Create your models here.
from accounts.models import User
from consultations.models import Consultation
from core.mixins import FieldPermissionModelMixinV3
from groups.models import Group


class Teacher(FieldPermissionModelMixinV3,models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    # consultation = models.OneToOneField(
    #     to=Consultation,
    #     on_delete=models.CASCADE,
    #     null=True
    # )

    learn_groups = models.ManyToManyField(
        to=Group,
        related_name='teachers',
        null=True
    )

    academic_title = models.CharField(max_length=240, null=True, default="")
    academic_degree = models.CharField(max_length=240, null=True, default="")

    class Meta:
        permissions = (
            ('can_change_teacher_consultation', "Can change consultations"),
        )

    def full_name(self):
        return f'{self.user.last_name} {self.user.first_name}'

    def __str__(self):
        return f'{self.user.last_name} {self.user.first_name}'