

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView


class FirstPageView(TemplateView):
    def get(self, request):
        if self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('accounts:profile'))
        else:
            return render(
                request=request,
                template_name='index.html'
            )