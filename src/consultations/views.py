from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView

from consultations.models import Consultation


class ConsultationListView(LoginRequiredMixin, ListView, PermissionRequiredMixin):
    model = Consultation
    template_name = 'consultations/consultation.html'
    context_object_name = 'consultation'
