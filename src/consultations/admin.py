from django.contrib import admin

# Register your models here.
from consultations.models import Consultation

admin.site.register(Consultation)