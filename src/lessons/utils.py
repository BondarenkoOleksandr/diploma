from lessons.models import Mark, Lesson


def create_marks(student):
    group = student.group
    for week in range(group.semester_weeks):
        for lesson in group.lessons_group.all():
            obj = Mark(
                lesson=lesson,
                group=group,
                subject=lesson.subject,
                teacher=lesson.teacher,
                student=student.user,
                week=week,
            )
            obj.save()


def get_weeks_colspan(lessons, group):
    colspan_weeks = []

    mode_count = {}
    for mode in Lesson.MODE_OF_LESSON:
        mode_count.update({mode[0]: lessons.filter(mode=mode[0]).count()})

    if mode_count[1] != 0 and mode_count[2] != 0:
        for week in range(1, group.semester_weeks + 1):
            if week % 2 == 0:
                colspan_weeks.append([week, mode_count[0] + mode_count[1]])
            else:
                colspan_weeks.append([week, mode_count[0] + mode_count[2]])
    elif mode_count[1] != 0:
        for week in range(1, group.semester_weeks + 1):
            if week % 2 == 0:
                colspan_weeks.append([week, mode_count[0] + mode_count[1]])
            elif mode_count[0] !=0:
                colspan_weeks.append([week, mode_count[0]])
    elif mode_count[2] != 0:
        for week in range(1, group.semester_weeks + 1):
            if week % 2 != 0:
                colspan_weeks.append([week, mode_count[0] + mode_count[2]])
            elif mode_count[0] !=0:
                colspan_weeks.append([week, mode_count[0]])
    else:
        for week in range(1, group.semester_weeks + 1):
            colspan_weeks.append([week, mode_count[0]])

    return colspan_weeks

def create_marks(instance):
    for week in range(1, instance.group.semester_weeks + 1):
        if instance.group.students.all():
            for student in instance.group.students.all():
                obj = Mark(
                    lesson=instance,
                    group=instance.group,
                    subject=instance.subject,
                    teacher=instance.teacher,
                    student=student.user,
                    week=week,
                )
                if instance.mode == 0:
                    obj.save()
                elif instance.mode == 1 and week % 2 == 0:
                    obj.save()
                elif instance.mode == 2 and week % 2 != 0:
                    obj.save()