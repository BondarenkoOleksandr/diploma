from django.db.models.signals import post_save, post_delete, pre_save
from django.dispatch import receiver

from lessons.models import Lesson, Mark
from lessons.utils import create_marks
from teachers.models import Teacher


@receiver(post_delete, sender=Lesson)
def delete_lesson(sender, instance, **kwargs):
    if not Lesson.objects.filter(group=instance.group, teacher=instance.teacher):
        teacher = Teacher.objects.get(user=instance.teacher)
        teacher.learn_groups.remove(instance.group)
        teacher.save()

@receiver(pre_save, sender=Lesson)
def edit_lesson_marks(sender, instance, **kwargs):
    marks = Mark.objects.filter(group=instance.group, lesson__uuid=instance.uuid)
    for mark in marks:
        mark.subject=instance.subject
        mark.teacher=instance.teacher
        mark.save()

@receiver(post_save, sender=Lesson)
def save_lesson(sender, instance, created, **kwargs):
    if created:
        create_marks(instance)
        teacher = Teacher.objects.get(user=instance.teacher)
        teacher.learn_groups.add(instance.group)
        teacher.save()
    else:
        marks = Mark.objects.filter(group=instance.group, lesson__uuid=instance.uuid)
        if instance.group.students.all():
            for student in instance.group.students.all():
                if (instance.mode==0 and marks.filter(student=student.user).count() !=15) or (instance.mode==1 and marks.filter(student=student.user).count() !=7) or (instance.mode==2 and marks.filter(student=student.user).count() !=8):
                    marks.delete()
                    create_marks(instance)