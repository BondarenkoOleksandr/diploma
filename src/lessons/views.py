from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render  # noqa
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView

from groups.models import Group
from lessons.forms import LessonCreateForm, LessonUpdateForm, MarkUpdateForm
from lessons.models import Lesson, Mark


class LessonCreateView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    model = Lesson
    form_class = LessonCreateForm
    template_name = 'lessons/lesson-create.html'
    success_url = reverse_lazy('index')
    permission_required = (
    'lessons.delete_lesson', 'lessons.view_lesson', 'lessons.add_lesson', 'lessons.change_lesson',
    'lessons.add_subject')

    def post(self, request):
        group = Group.objects.get(id=self.request.POST.get('group'))
        form = LessonCreateForm(request.POST, group_uuid=group.uuid)

        if form.is_valid():
            form.save()
        else:
            return HttpResponse(form.errors.as_json(), status=400)
            # return JsonResponse(data=form.errors.as_json(), status=500)

        data = {'lesson_uuid': form.instance.uuid, 'day': form.instance.get_days(), 'days': form.instance.days,
                'type': form.instance.get_type(),
                'group': form.instance.group.id, 'teacher': form.instance.teacher.get_full_name(),
                'room': form.instance.room, 'name': form.instance.subject.name, 'time': form.instance.time,
                'mode': form.instance.mode}

        return JsonResponse(data)


class LessonUpdateView(LoginRequiredMixin, UpdateView, PermissionRequiredMixin):
    # model = Lesson
    # form_class = LessonUpdateForm
    # template_name = 'lessons/lesson-update.html'
    # pk_url_kwarg = 'lesson_uuid'
    success_url = reverse_lazy('index')

    def get(self, request, lesson_uuid):
        lesson = Lesson.objects.get(uuid=lesson_uuid)
        form = LessonUpdateForm(instance=lesson, group_uuid=lesson.group.uuid)
        return render(
            request=request,
            template_name='lessons/lesson-update.html',
            context={
                'form': form,
            }
        )

    def post(self, request, lesson_uuid):
        lesson = Lesson.objects.get(uuid=lesson_uuid)
        form = LessonUpdateForm(request.POST, instance=lesson, group_uuid=lesson.group.uuid)

        if form.is_valid():
            form.save()
        else:
            return HttpResponse(form.errors.as_json(), status=400)

        data = {'lesson_uuid': lesson_uuid, 'day': lesson.get_days(), 'days': lesson.days, 'type': lesson.get_type(),
                'group': lesson.group.id, 'teacher': lesson.teacher.get_full_name(),
                'room': lesson.room, 'name': lesson.subject.name, 'time': lesson.time, 'mode': lesson.mode}

        return JsonResponse(data)


class LessonDeleteView(LoginRequiredMixin, DeleteView, PermissionRequiredMixin):
    model = Lesson
    form_class = LessonCreateForm
    template_name = 'lessons/lesson-delete.html'
    pk_url_kwarg = 'lesson_uuid'
    success_url = reverse_lazy('index')
    permission_required = ['']

    def post(self, request, lesson_uuid, *args, **kwargs):
        Lesson.objects.get(uuid=lesson_uuid).delete()

        data = {'lesson_uuid': lesson_uuid}

        return JsonResponse(data)


class MarkUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    permission_required = 'lessons.change_mark'
    model = Mark
    form_class = MarkUpdateForm
    template_name = 'marks/mark-update.html'
    pk_url_kwarg = 'mark_id'
    success_url = reverse_lazy('index')
