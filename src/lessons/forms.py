from django.core.exceptions import ValidationError
from django.forms import ModelForm, ChoiceField

from accounts.models import User
from groups.models import Group
from lessons.models import Lesson, Mark, Subject
from teachers.models import Teacher


class LessonBaseForm(ModelForm):
    class Meta:
        model = Lesson
        fields = '__all__'


class LessonCreateForm(LessonBaseForm):
    class Meta(LessonBaseForm):
        model = Lesson
        exclude = ['teacher', 'uuid', 'position', 'subject']

    def __init__(self, *args, **kwargs):
        group_uuid = kwargs.pop('group_uuid', None)
        print(group_uuid)
        super(LessonCreateForm, self).__init__(*args, **kwargs)
        group = Group.objects.get(uuid=group_uuid)
        teachers = [(tch.user.id, tch.user.get_full_name()) for tch in Teacher.objects.all()]
        subjects = [(sbj.id, sbj.name) for sbj in group.subjects.all()]
        self.fields['teacher_field'] = ChoiceField(choices=teachers)
        self.fields['subject_field'] = ChoiceField(choices=subjects)
        teacher_id = 0
        if self.instance.teacher:
            teacher_id = self.instance.teacher.id

        self.fields['teacher_field'].initial = [teacher_id]

    def clean(self):
        super(LessonCreateForm, self).clean()
        lessons = Lesson.objects.filter(group=self.cleaned_data['group'], time=self.cleaned_data['time'],
                                        days=self.cleaned_data['days'])
        if lessons:
            for lesson in lessons:
                if lesson.mode == self.cleaned_data['mode']:
                    raise ValidationError("Помилка!"+lesson.get_full_mode()+" в цей день та час уже існують!")
                elif lesson.type == self.cleaned_data['type'] and lesson.subject == self.cleaned_data['subject_field']:
                    raise ValidationError("Помилка!Таке заняття вже є!")
                elif self.cleaned_data['mode'] == 0:
                    raise ValidationError("Помилка!Додати звичайне заняття неможливо! Спочатку видаліть всі заняття "
                                          "по парним/не парним тижням!")

    def save(self, commit=True):
        teacher_id = self.cleaned_data['teacher_field']
        subject_id = self.cleaned_data['subject_field']
        self.instance.teacher = User.objects.get(id=teacher_id)
        self.instance.subject = Subject.objects.get(id=subject_id)
        return super().save(commit=True)


class LessonUpdateForm(LessonBaseForm):
    class Meta(LessonBaseForm.Meta):
        exclude = ['uuid', 'teacher', 'position', 'subject']

    def __init__(self, *args, **kwargs):
        group_uuid = kwargs.pop('group_uuid', None)
        super(LessonUpdateForm, self).__init__(*args, **kwargs)
        group = Group.objects.get(uuid=group_uuid)
        teachers = [(tch.user.id, tch.user.get_full_name()) for tch in Teacher.objects.all()]
        subjects = [(sbj.id, sbj.name) for sbj in group.subjects.all()]
        self.fields['teacher_field'] = ChoiceField(choices=teachers)
        self.fields['subject_field'] = ChoiceField(choices=subjects)
        teacher_id = 0
        if self.instance.teacher:
            teacher_id = self.instance.teacher.id

        self.fields['teacher_field'].initial = [teacher_id]

    def clean(self):
        super(LessonUpdateForm, self).clean()
        lessons = Lesson.objects.filter(group=self.cleaned_data['group'], time=self.cleaned_data['time'],
                                        days=self.cleaned_data['days']).exclude(uuid=self.instance.uuid)
        if lessons:
            for lesson in lessons:
                if lesson.mode == self.cleaned_data['mode']:
                    raise ValidationError("Помилка!" + lesson.get_full_mode() + " в цей день та час уже існують!")
                elif lesson.type == self.cleaned_data['type'] and lesson.subject == self.cleaned_data['subject_field']:
                    raise ValidationError("Помилка!Таке заняття вже є!")
                elif self.cleaned_data['mode'] == 0:
                    raise ValidationError("Помилка!Додати звичайне заняття неможливо! Спочатку видаліть всі заняття "
                                          "по парним/не парним тижням!")

    def save(self, commit=True):
        teacher_id = self.cleaned_data['teacher_field']
        subject_id = self.cleaned_data['subject_field']
        self.instance.teacher = User.objects.get(id=teacher_id)
        self.instance.subject = Subject.objects.get(id=subject_id)
        return super().save(commit=True)


class MarkBaseForm(ModelForm):
    class Meta:
        model = Mark
        fields = '__all__'


class MarkUpdateForm(MarkBaseForm):
    class Meta(MarkBaseForm.Meta):
        fields = ['points']
